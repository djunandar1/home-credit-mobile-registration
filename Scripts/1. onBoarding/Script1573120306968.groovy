import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

Mobile.startApplication('C:\\Users\\User\\Downloads\\id.co.myhomecredit_3.2.8.apk', true)

Mobile.waitForElementPresent(findTestObject('Onboarding/android.widget.ImageView0 imageSelamatDatang1'), 0)

WebUI.delay(1, FailureHandling.STOP_ON_FAILURE)

Mobile.swipe(600, 300, 10, 300)

Mobile.waitForElementPresent(findTestObject('Onboarding/android.widget.ImageView0 imageProductCiamik'), 0)

WebUI.delay(1, FailureHandling.STOP_ON_FAILURE)

Mobile.swipe(600, 300, 10, 300)

Mobile.waitForElementPresent(findTestObject('Onboarding/android.widget.ImageView0 imageProsesCepat'), 0)

Mobile.delay(1, FailureHandling.STOP_ON_FAILURE)

Mobile.swipe(600, 300, 10, 300)

Mobile.waitForElementPresent(findTestObject('Onboarding/android.widget.ImageView0 imagePromo'), 0)

Mobile.delay(1, FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('Onboarding/buttonDaftar'), 0)

Mobile.waitForElementPresent(findTestObject('Registration/android.widget.Button0 - LANJUTKAN'), 0)

Mobile.delay(2, FailureHandling.STOP_ON_FAILURE)

