import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

Mobile.verifyElementVisible(findTestObject('Agreement/android.widget.TextView0 - Header Persetujuan Penggunaan Data Pribadi dan Fasilitas Pembiayaan'), 
    0, FailureHandling.CONTINUE_ON_FAILURE)

Mobile.verifyElementVisible(findTestObject('Agreement/android.widget.TextView0 - Saya Setuju'), 0)

Mobile.verifyElementVisible(findTestObject('Agreement/android.widget.TextView0 - Kembali (persetujuan)'), 0, FailureHandling.CONTINUE_ON_FAILURE)

Mobile.scrollToText('otoritas pemerintah terkait sesuai dengan peratura perundang-undangan yang berlaku.', FailureHandling.CONTINUE_ON_FAILURE)

Mobile.delay(1, FailureHandling.STOP_ON_FAILURE)

Mobile.verifyElementExist(findTestObject('Agreement/android.widget.TextView0 - Saya Setuju'), 0)

Mobile.tap(findTestObject('Agreement/android.widget.TextView0 - Saya Setuju'), 0)

not_run: Mobile.waitForElementPresent(findTestObject('OTP/android.widget.EditText0 input OTP'), 0)

not_run: Mobile.openNotifications()

not_run: String message = Mobile.getText(findTestObject('Object Repository/OTP/android.widget.TextView0 - Kode OTP'), 0, 
    FailureHandling.CONTINUE_ON_FAILURE)

not_run: Mobile.closeNotifications()

not_run: Mobile.sendKeys(findTestObject('OTP/android.widget.EditText0 inputOTP'), message.replaceAll('Kode OTP untuk verifikasi anda adalah ', 
        ''))

not_run: Mobile.tap(findTestObject('OTP/android.widget.Button0 - DAFTAR OTP'), 0)

not_run: Mobile.verifyElementVisible(findTestObject('Registration/android.widget.TextView0 - Pendaftaran BerhasilTerima Kasih'), 
    0)

not_run: Mobile.tap(findTestObject('Registration/android.widget.Button0 - buttonOK - success'), 0)

not_run: Mobile.verifyElementVisible(findTestObject('5. Homescreen/android.widget.TextView0 - home screen Versi aplikasi terbaru sudah tersedia Anda harus memperbarui agar dapat melanjutkan penggunaan aplikasi.'), 
    0)

not_run: Mobile.tap(findTestObject('5. Homescreen/android.widget.TextView0 - home screen Versi aplikasi terbaru sudah tersedia Anda harus memperbarui agar dapat melanjutkan penggunaan aplikasi.'), 
    0)

