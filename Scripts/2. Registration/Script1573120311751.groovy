import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

Mobile.verifyElementVisible(findTestObject('Registration/android.widget.TextView0 - Tanggal Lahir'), 0)

Mobile.tap(findTestObject('Registration/android.widget.Button0 - LANJUTKAN'), 0)

Mobile.verifyElementVisible(findTestObject('Registration/android.widget.TextView0 - Anda belum mengisi tanggal lahir'), 
    0)

Mobile.tap(findTestObject('Registration/android.widget.Button0 - OK (Tanggal)'), 0)

Mobile.tap(findTestObject('Registration/android.widget.TextView0 - input Tanggal Lahir'), 0)

Mobile.tap(findTestObject('Registration/android.widget.Button0 - OK (Tanggal)'), 0)

not_run: CustomKeywords.'com.hci.keyword.Datepicker.handleDatepicker'(findTestObject('Registration/android.widget.TextView0 - input Tanggal Lahir'), 
    '1', '2', '2000')

Mobile.sendKeys(findTestObject('Registration/android.widget.EditText0 - Masukkan No. Handphone Anda'), GlobalVariable.phoneNumber, 
    FailureHandling.CONTINUE_ON_FAILURE)

Mobile.setText(findTestObject('Registration/android.widget.EditText0 - Tentukan Kode PIN (4 Digit)'), GlobalVariable.pinCode, 
    0)

Mobile.setText(findTestObject('Registration/android.widget.EditText0 - Konfirmasi Kode PIN'), GlobalVariable.pinConfirmation, 
    0)

Mobile.tap(findTestObject('Registration/android.widget.Button0 - LANJUTKAN'), 0)

