#Author: Didit Yunandar // djunandar@gmail.com
#Keywords Summary : Registration Summary
#Feature: List of scenarios.
#Scenario: Registration process in Home Credit Indonesia
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
@tag
Feature: Registration process in Home Credit Indonesia
  

  @tag1
  Scenario: I want to register to Home Credit Indonesia as a member with mobile apps
    Given I open the application and swipes all the onboarding screen
    When I completely the mandatory field in registration page
    And I scroll the page to agreed the Agreement
    Then I Success register in the  Mobile Apps
