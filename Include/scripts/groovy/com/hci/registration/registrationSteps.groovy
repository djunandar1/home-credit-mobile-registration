package com.hci.registration
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords

import internal.GlobalVariable

import MobileBuiltInKeywords as Mobile
import WSBuiltInKeywords as WS
import WebUiBuiltInKeywords as WebUI

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When


class registrationSteps {
	/**
	 * The step definitions below match with Katalon sample Gherkin steps
	 */
	@Given("I open the application and swipes all the onboarding screen")
	def GivenSteps() {
		
		Mobile.startApplication('C:\\Users\\User\\Downloads\\id.co.myhomecredit_3.2.8.apk', true)
		
		Mobile.waitForElementPresent(findTestObject('Onboarding/android.widget.ImageView0 imageSelamatDatang1'), 0)
		
		WebUI.delay(1, FailureHandling.STOP_ON_FAILURE)
		
		Mobile.swipe(600, 300, 10, 300)
		
		Mobile.waitForElementPresent(findTestObject('Onboarding/android.widget.ImageView0 imageProductCiamik'), 0)
		
		WebUI.delay(1, FailureHandling.STOP_ON_FAILURE)
		
		Mobile.swipe(600, 300, 10, 300)
		
		Mobile.waitForElementPresent(findTestObject('Onboarding/android.widget.ImageView0 imageProsesCepat'), 0)
		
		Mobile.delay(1, FailureHandling.STOP_ON_FAILURE)
		
		Mobile.swipe(600, 300, 10, 300)
		
		Mobile.waitForElementPresent(findTestObject('Onboarding/android.widget.ImageView0 imagePromo'), 0)
		
		Mobile.delay(1, FailureHandling.STOP_ON_FAILURE)
		
		Mobile.tap(findTestObject('Onboarding/buttonDaftar'), 0)
		
		Mobile.waitForElementPresent(findTestObject('Registration/android.widget.Button0 - LANJUTKAN'), 0)
		
		Mobile.delay(2, FailureHandling.STOP_ON_FAILURE)
					
		
	}

	@When("I completely the mandatory field in registration page")
	def WhenSteps() {
		
		Mobile.verifyElementVisible(findTestObject('Registration/android.widget.TextView0 - Tanggal Lahir'), 0)
		
		Mobile.tap(findTestObject('Registration/android.widget.Button0 - LANJUTKAN'), 0)
		
		Mobile.verifyElementVisible(findTestObject('Registration/android.widget.TextView0 - Anda belum mengisi tanggal lahir'),
			0)
		
		Mobile.tap(findTestObject('Registration/android.widget.Button0 - OK (Tanggal)'), 0)
		
		Mobile.tap(findTestObject('Registration/android.widget.TextView0 - input Tanggal Lahir'), 0)
		
		Mobile.tap(findTestObject('Registration/android.widget.Button0 - OK (Tanggal)'), 0)
		
			
		Mobile.sendKeys(findTestObject('Registration/android.widget.EditText0 - Masukkan No. Handphone Anda'), GlobalVariable.phoneNumber,
			FailureHandling.CONTINUE_ON_FAILURE)
		
		Mobile.setText(findTestObject('Registration/android.widget.EditText0 - Tentukan Kode PIN (4 Digit)'), GlobalVariable.pinCode,
			0)
		
		Mobile.setText(findTestObject('Registration/android.widget.EditText0 - Konfirmasi Kode PIN'), GlobalVariable.pinConfirmation,
			0)
		
		Mobile.tap(findTestObject('Registration/android.widget.Button0 - LANJUTKAN'), 0)
		
		
		
	}

	@And("I scroll the page to agreed the Agreement")
	def AndSteps() {
		Mobile.verifyElementVisible(findTestObject('Agreement/android.widget.TextView0 - Header Persetujuan Penggunaan Data Pribadi dan Fasilitas Pembiayaan'),
			0, FailureHandling.CONTINUE_ON_FAILURE)
		
		Mobile.verifyElementVisible(findTestObject('Agreement/android.widget.TextView0 - Saya Setuju'), 0)
		
		Mobile.verifyElementVisible(findTestObject('Agreement/android.widget.TextView0 - Kembali (persetujuan)'), 0, FailureHandling.CONTINUE_ON_FAILURE)
		
		Mobile.scrollToText('otoritas pemerintah terkait sesuai dengan peratura perundang-undangan yang berlaku.', FailureHandling.CONTINUE_ON_FAILURE)
		
		Mobile.delay(1, FailureHandling.STOP_ON_FAILURE)
		
		Mobile.verifyElementExist(findTestObject('Agreement/android.widget.TextView0 - Saya Setuju'), 0)
		
		Mobile.tap(findTestObject('Agreement/android.widget.TextView0 - Saya Setuju'), 0)
		
		Mobile.waitForElementPresent(findTestObject('OTP/android.widget.EditText0 input OTP'), 0)
		
		Mobile.openNotifications()
		
		String message = Mobile.getText(findTestObject('Object Repository/OTP/android.widget.TextView0 - Kode OTP'), 0, FailureHandling.CONTINUE_ON_FAILURE)
		
		Mobile.closeNotifications()
		
		Mobile.sendKeys(findTestObject('OTP/android.widget.EditText0 inputOTP'), message.replaceAll('Kode OTP untuk verifikasi anda adalah ',
				''))
		
		Mobile.tap(findTestObject('OTP/android.widget.Button0 - DAFTAR OTP'), 0)
		
				

	}
	
	@Then("I Success register in the  Mobile Apps")
	def ThenSteps() {
		Mobile.verifyElementVisible(findTestObject('Registration/android.widget.TextView0 - Pendaftaran BerhasilTerima Kasih'),
			0)
		
		Mobile.tap(findTestObject('Registration/android.widget.Button0 - buttonOK - success'), 0)
		
		Mobile.verifyElementVisible(findTestObject('5. Homescreen/android.widget.TextView0 - home screen Versi aplikasi terbaru sudah tersedia Anda harus memperbarui agar dapat melanjutkan penggunaan aplikasi.'),
			0)
		
		Mobile.tap(findTestObject('5. Homescreen/android.widget.TextView0 - home screen Versi aplikasi terbaru sudah tersedia Anda harus memperbarui agar dapat melanjutkan penggunaan aplikasi.'),
			0)
		
		
		
	}
}