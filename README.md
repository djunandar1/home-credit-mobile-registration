# Hi, welcome! :D
## Readme file
----
Author: Didit Yunandar djunandar@gmail.com

>This readme file contain automation script for Registration process in Home Credit Indonesia. Automation Script can be running in Katalon Studio and detected Android device.
Before running please update phone number in testDate.xls file
Because the phone number is already registered.

## Scenario
Please create the automation script using Appium or Espresso for My Homecredit Application. The scope are:

1.	Install My homecredit mobile app from play store here
2.	Open the app
3.	Swipe left / swipe right the page in on boarding page after app installed
4.	Click on button “Daftar” to register
5.	Fill in date of birth, phone number, PIN code, and PIN confirmation , then click on button “Lanjutkan”
6.	Check existence of consent page (halaman syarat dan ketentuan) then click “I Agree”
7.	Read OTP code
8.	Click “Daftar” until register sucesss and Homepage of My Homecredit App is opened

It would be plus point if you create the test script for Android and iOs version. But in this requirement, you can make the priority for Android version.


----
## usage
1. Learn how registration in Home Credit Indonesia
2. Learning how BDD process works in Katalon studio